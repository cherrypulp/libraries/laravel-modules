<?php

namespace Blok\Modules;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    const CONFIG_PATH = __DIR__ . '/../config/modules.php';

    public function boot()
    {
        $this->publishes([
            self::CONFIG_PATH => config_path('modules.php'),
        ], 'config');
    }

    public function register()
    {
        $this->mergeConfigFrom(
            self::CONFIG_PATH,
            'modules'
        );

        $this->app->bind('modules', function () {
            return new Modules();
        });
    }
}
