# Blok Modules

[![Build Status](https://travis-ci.org/blok/modules.svg?branch=master)](https://travis-ci.org/blok/modules)
[![styleci](https://styleci.io/repos/CHANGEME/shield)](https://styleci.io/repos/CHANGEME)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/g/blok/modules/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/g/blok/modules/?branch=master)
[![SensioLabsInsight](https://insight.sensiolabs.com/projects/CHANGEME/mini.png)](https://insight.sensiolabs.com/projects/CHANGEME)
[![Coverage Status](https://coveralls.io/repos/github/blok/modules/badge.svg?branch=master)](https://coveralls.io/github/blok/modules?branch=master)

[![Packagist](https://img.shields.io/packagist/v/blok/modules.svg)](https://packagist.org/packages/blok/modules)
[![Packagist](https://poser.pugx.org/blok/modules/d/total.svg)](https://packagist.org/packages/blok/modules)
[![Packagist](https://img.shields.io/packagist/l/blok/modules.svg)](https://packagist.org/packages/blok/modules)

Override the default templates and config of Nwidart Laravel Modules

## Installation

Install via composer
```bash
composer require blok/modules
```

### Register Service Provider

**Note! This and next step are optional if you use laravel>=5.5 with package
auto discovery feature.**

Add service provider to `config/app.php` in `providers` section
```php
Blok\Modules\ServiceProvider::class,
```

### Register Facade

Register package facade in `config/app.php` in `aliases` section
```php
Blok\Modules\Facades\Modules::class,
```

### Publish Configuration File

```bash
php artisan vendor:publish --provider="Blok\Modules\ServiceProvider" --tag="config"
```

## Usage

For more information please check :

https://github.com/nWidart/laravel-modules

## Security

If you discover any security related issues, please email
instead of using the issue tracker.

## Credits

- [Cherrypulp](https://gitlab.com/cherrypulp/libraries/laravel-modules)
- [All contributors](https://github.com/blok/modules/graphs/contributors)

This package is bootstrapped with the help of
[cherrypulp/laravel-package-generator](https://github.com/cherrypulp/laravel-package-generator).
