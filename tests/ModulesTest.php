<?php

namespace Blok\Modules\Tests;

use Blok\Modules\Facades\Modules;
use Blok\Modules\ServiceProvider;
use Orchestra\Testbench\TestCase;

class ModulesTest extends TestCase
{
    protected function getPackageProviders($app)
    {
        return [ServiceProvider::class];
    }

    protected function getPackageAliases($app)
    {
        return [
            'modules' => Modules::class,
        ];
    }

    public function testExample()
    {
        $this->assertEquals(1, 1);
    }
}
